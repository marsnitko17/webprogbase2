const User = require("../models/user.js");

module.exports = function (app, passport) {

  app.get('/users', checkAuth, checkAdmin, function (req, res) {
    let searchStr = req.query.search;
    if (!searchStr) searchStr = "";
    if (!req.query.page) req.query.page = 0;
    const lm = 3;
    let skp = req.query.page * lm;
    let entitiesCount = 0;

    User.schema.methods.getUsersCount(searchStr)
      .then(count => {
        entitiesCount = count;
        return User.schema.methods.getAll2(searchStr, skp, lm);
      })
      .then(users => {
        let maxPage = Math.ceil(entitiesCount / 3);
        let maxPageOnSite = maxPage;
        const pageOnSite = +req.query.page + 1;
        let prevPage = +req.query.page - 1;
        let nextPage = +req.query.page + 1;
        let right, left = false;
        if (prevPage < 0) left = false;
        else left = true;
        if (nextPage >= maxPage) right = false;
        else right = true;
        if (maxPage > 1) maxPage = maxPage - 1;
        if (maxPageOnSite === 0) maxPageOnSite = 1;
        res.render('users', { pageOnSite, right, left, prevPage, nextPage, maxPageOnSite, searchStr, count: entitiesCount, users: users, user: req.user, login2: req.user.login });
      })
      .catch(err => res.status(500).send(err.toString()));
  });

  app.get('/users/:id', checkAuth, checkAdmin, function (req, res) {
    const id = req.params.id;
    User.schema.methods.getById(id)
      .then(user => {
        if (typeof user === 'undefined') {
          res.render('error');
        }
        else {
          res.render('user', { user: user, login2: req.user.login });
        }
      })
      .catch(err => res.status(500).send(err.toString()));
  });

  app.get('/profile', checkAuth, function (req, res) {
    const id = req.user.id;
    User.schema.methods.getById(id)
      .then(user => {
        if (typeof user === 'undefined') {
          res.render('error');
        }
        else {
          res.render('profile', user);
        }
      })
      .catch(err => res.status(500).send(err.toString()));
  });

  app.post('/users/:id', checkAuth, checkAdmin, function (req, res) {
    let str = req.url;
    let arr = str.split("/", 3);
    let id = arr.pop();
    const element = {
      id: id,
      role: req.body.role
    };
    User.schema.methods.update(element)
      .then((user) => res.redirect(`/users/${user.id}`))
      .catch(err => res.status(500).send(err.toString()));
  });
};

function checkAdmin(req, res, next) {
  if (req.user.role !== 1) return res.sendStatus(403);
  next();
}

function checkAuth(req, res, next) {
  if (!req.user) return res.render('unauth');
  next();
}